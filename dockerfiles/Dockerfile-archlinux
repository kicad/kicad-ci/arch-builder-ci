FROM archlinux:latest

ARG PACKAGES
ARG USER_NAME

RUN dbus-uuidgen --ensure=/etc/machine-id

# Clear cache to save space
RUN pacman -Syu --noconfirm --noprogressbar --disable-download-timeout --needed ${PACKAGES} \
 && rm -r /var/cache/pacman/pkg/* && rm -r /var/lib/pacman/sync/*

# Add a non-root user
RUN useradd -m ${USER_NAME} && echo "${USER_NAME} ALL = NOPASSWD: /usr/bin/pacman" > /etc/sudoers.d/${USER_NAME}-pacman

# Setup ssh and add useful known hosts, keys will be added to the container at runtime
RUN eval $(ssh-agent -s) && \
	mkdir -p ~/.ssh && \
	chmod 700 ~/.ssh && \
	ssh-keyscan aur.archlinux.org >> ~/.ssh/known_hosts && \
	ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && \
	chmod 644 ~/.ssh/known_hosts

# Git defaults and workaround for safe directory in docker container
RUN git config --global --add safe.directory '*' && \
	git config --global --add user.name "${USER_NAME}" && \
	git config --global --add user.email "noreply@kicad.org"
